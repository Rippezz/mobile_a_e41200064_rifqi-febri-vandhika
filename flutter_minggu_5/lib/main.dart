import 'package:flutter/material.dart';
import 'package:flutter_minggu_5/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
