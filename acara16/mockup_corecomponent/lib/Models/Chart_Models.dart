class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Kagura',
      message: 'Hello Kagura',
      time: '19.00',
      profileUrl:
          'https://static.wikia.nocookie.net/mobile-legends/images/9/9d/Onmyouji_Master_%28rework%29.jpg'),
  ChartModel(
      name: 'Lunox',
      message: 'Hello Lunox',
      time: '14.21',
      profileUrl:
          'https://64.media.tumblr.com/437febbd89ca7f1809bdee67f3e77bf7/37b2dc0ef9d0ed9c-a0/s500x750/8e1d3e480a9ad37f51c140b97065e07c0c1be849.jpg'),
  ChartModel(
      name: 'Eudora',
      message: 'Hello Eudora',
      time: '13.41',
      profileUrl:
          'https://4.bp.blogspot.com/-K6E3gykSVHA/W1HBV2tmn-I/AAAAAAAADfY/38PgfIsqQVA-dYnN5oWEhaVUeoUba9PmACLcBGAs/s1600/eudora.png'),
  ChartModel(
      name: 'Valir',
      message: 'Hello Valir',
      time: '12.00',
      profileUrl:
          'https://api.duniagames.co.id/api/content/upload/file/17100047561588924138.jpg'),
  ChartModel(
      name: 'Vale',
      message: 'Hello Vale',
      time: '11.00',
      profileUrl:
          'https://2.bp.blogspot.com/-GrbQFqk2efo/W4I2qHq7ZMI/AAAAAAAAEAU/xp3c8JEgX0w32WLTJZ3YUQB3tld1JUc6wCLcBGAs/s1600/vale%2Bquotes%2Bmobile%2Blegends.png'),
  ChartModel(
      name: 'Ling',
      message: 'Hello Ling',
      time: '09.21',
      profileUrl:
          'https://static.wikia.nocookie.net/mobile-legends/images/f/fa/Cyan_Finch.jpg'),
  ChartModel(
      name: 'Natalia',
      message: 'Hello Natalia',
      time: '08.41',
      profileUrl:
          'https://cdna.artstation.com/p/assets/images/images/014/659/690/large/neelam-ayuningrum-asset.jpg'),
  ChartModel(
      name: 'Gusion',
      message: 'Hello Gusion',
      time: '22 february',
      profileUrl:
          'https://static.wikia.nocookie.net/mobile-legends/images/9/90/Holy_Blade.jpg'),
  ChartModel(
      name: 'Selena',
      message: 'Hello Selena',
      time: '23 february',
      profileUrl:
          'https://static.wikia.nocookie.net/mobile-legends/images/a/ae/Abyssal_Witch.jpg'),
];
